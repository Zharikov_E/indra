package application.indra.gathering.harvest;

import application.indra.gathering.Harvest;
import application.indra.gathering.PropertiesSet;
import application.indra.gathering.properties.BasicPropertiesSet;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

public class BasicHarvest implements Harvest {

    private Map<String, PropertiesSet> propertyCategories = new LinkedHashMap<>();

    @Override
    public void setCategory(String category, PropertiesSet props) {
        propertyCategories.put(category, props);
    }

    @Override
    public PropertiesSet getCategoryProperties(String category) {
        if (!propertyCategories.containsKey(category)) {
            propertyCategories.put(category, new BasicPropertiesSet());
        }
        return propertyCategories.get(category);
    }

    @Override
    public void removeCategory(String category) {
        propertyCategories.remove(category);
    }

    @Override
    public Set<String> getCategoryNames() {
        return propertyCategories.keySet();
    }

    @Override
    public Map<String, PropertiesSet> getPropertyCategories() {
        return propertyCategories;
    }
}
