package application.indra.plugin.embed;

import application.indra.gathering.gatherer.software.ProcessesGatherer;
import application.indra.plugin.BasicPlugin;

public class ProcessesPlugin extends BasicPlugin {

    public ProcessesPlugin() {
        super("Processes");
        addGatherer(new ProcessesGatherer());
    }
}
