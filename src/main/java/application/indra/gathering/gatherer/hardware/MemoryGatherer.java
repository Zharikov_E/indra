package application.indra.gathering.gatherer.hardware;

import application.indra.gathering.PropertiesSet;
import application.indra.gathering.gatherer.GenericGatherer;
import oshi.hardware.GlobalMemory;

public class MemoryGatherer extends GenericGatherer {

    @Override
    public void gather() {
        final GlobalMemory memory = HARDWARE.getMemory();
        gatherMemory(memory);
        logProperties();
    }

    protected void gatherMemory(final GlobalMemory memory) {
        PropertiesSet memoryProps = harvest.getCategoryProperties("memory");
        memoryProps.setProperty("available", memoryInMB(memory.getAvailable()));
        memoryProps.setProperty("page-size", memoryInMB(memory.getPageSize()));
        memoryProps.setProperty("swap-pages-in", memoryInMB(memory.getSwapPagesIn()));
        memoryProps.setProperty("swap-pages-out", memoryInMB(memory.getSwapPagesOut()));
        memoryProps.setProperty("swap-total", memoryInMB(memory.getSwapTotal()));
        memoryProps.setProperty("swap-used", memoryInMB(memory.getSwapUsed()));
        memoryProps.setProperty("total", memoryInMB(memory.getTotal()));
    }

}
