package application.indra.gathering.gatherer.hardware;

import application.indra.gathering.PropertiesSet;
import application.indra.gathering.gatherer.GenericGatherer;
import oshi.hardware.UsbDevice;

public class USBDevicesGatherer extends GenericGatherer {

    public USBDevicesGatherer() {
    }

    private USBDevicesGatherer(UsbDevice[] devices) {
        gatherUsbDevices(devices);
    }

    @Override
    public void gather() {
        gatherUsbDevices( HARDWARE.getUsbDevices(true));
        logProperties();
    }

    protected void gatherUsbDevices(final UsbDevice[] hwUsbDevices) {
        for (int i = 0; i < hwUsbDevices.length; i++) {
            final UsbDevice usbDevice = hwUsbDevices[i];

            PropertiesSet usbProps = harvest.getCategoryProperties("usb-device-" + i);

            usbProps.setProperty("name", usbDevice.getName());
            usbProps.setProperty("product-id", usbDevice.getProductId());
            usbProps.setProperty("serial-number", usbDevice.getSerialNumber());
            usbProps.setProperty("vendor", usbDevice.getVendor());
            usbProps.setProperty("vendor-id", usbDevice.getVendorId());
            final UsbDevice[] connectedDevices = usbDevice.getConnectedDevices();
            if (connectedDevices.length > 0) {
                usbProps.setProperty("connected-devices",
                        new USBDevicesGatherer(connectedDevices).getHarvest());
            }
        }
    }

}
