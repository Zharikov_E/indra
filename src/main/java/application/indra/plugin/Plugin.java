package application.indra.plugin;


import java.util.Map;

public interface Plugin {

    Map<String, Map<String, Object>> getInfo();

}
