package application.indra.gathering.gatherer.hardware;

import application.indra.gathering.PropertiesSet;
import application.indra.gathering.gatherer.GenericGatherer;
import oshi.hardware.NetworkIF;

import java.util.Arrays;

import static java.lang.System.setProperty;

public class NetworkInterfacesGatherer extends GenericGatherer {

    public NetworkInterfacesGatherer() {

    }

    @Override
    public void gather() {
        gatherNetworkInterfaces(HARDWARE.getNetworkIFs());
        logProperties();
    }

    protected void gatherNetworkInterfaces(final NetworkIF[] networkIFs) {
        for (int i = 0; i < networkIFs.length; i++) {
            final NetworkIF network = networkIFs[i];

            PropertiesSet networkProps = harvest.getCategoryProperties("network-interface-" + i);
            networkProps.setProperty("name", network.getName());
            networkProps.setProperty("display-name", network.getDisplayName());
            networkProps.setProperty("mac-address", network.getMacaddr());
            networkProps.setProperty("ipv4-address", Arrays.toString(network.getIPv4addr()));
            networkProps.setProperty("ipv6-address", Arrays.toString(network.getIPv4addr()));
            networkProps.setProperty("mtu", network.getMTU());
            networkProps.setProperty("speed", network.getSpeed());
            networkProps.setProperty("timestamp", network.getTimeStamp());
        }
    }

}
