package application.indra.plugin.formatter;

import application.indra.gathering.Gatherer;
import application.indra.gathering.Harvest;
import application.indra.plugin.Formatter;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.TreeMap;

public class BasicTextFormatter implements Formatter {

    @Override
    public Map<String, Map<String, Object>> format(Gatherer gatherer) {
        Map<String, Map<String, Object>> formattedInfo = new TreeMap<>();

        Harvest harvest = gatherer.getHarvest();

        harvest.getPropertyCategories().forEach((category, properties) -> {
            Map<String, Object> formattedProperties = new LinkedHashMap<>();

            properties.getProperties().forEach((propName, value) -> {
                formattedProperties.put(propName.replaceAll("-", " ") + ":", value);
            });

            formattedInfo.put(category.replaceAll("-", " "), formattedProperties);
        });

        return formattedInfo;
    }

}
