package application;

import application.indra.plugin.embed.*;
import application.indra.reporting.reporter.OutputStreamReporter;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.LinkedHashSet;

@SpringBootApplication
public class IndraApplication {

    public static void main(String[] args) {
        if (args.length > 0 && args[0].trim().toLowerCase().equals("-file")) {

            File reportFile = new File("report.txt");
            try (OutputStream os = new FileOutputStream(reportFile)) {
                OutputStreamReporter outputStreamReporter =
                        new OutputStreamReporter(os, new LinkedHashSet<>(Arrays.asList(
                                new CPUPlugin(),
                                new DiskStoresPlugin(),
                                new DisplayPlugin(),
                                new FileSystemPlugin(),
                                new NetworkingPlugin(),
                                new PCSystemPlugin(),
                                new SoundCardPlugin()
                        )));
                outputStreamReporter.writeReport();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            SpringApplication.run(IndraApplication.class, args);
        }
    }

}
