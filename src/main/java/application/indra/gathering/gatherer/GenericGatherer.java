package application.indra.gathering.gatherer;

import application.indra.gathering.Gatherer;
import application.indra.gathering.Harvest;
import application.indra.gathering.harvest.BasicHarvest;
import application.indra.logging.Log;
import oshi.PlatformEnum;
import oshi.SystemInfo;
import oshi.hardware.HardwareAbstractionLayer;
import oshi.software.os.OperatingSystem;

public abstract class GenericGatherer implements Gatherer {

    protected static final SystemInfo SYSTEM_INFO = new SystemInfo();
    protected static final HardwareAbstractionLayer HARDWARE = SYSTEM_INFO.getHardware();
    protected static final OperatingSystem SOFTWARE = SYSTEM_INFO.getOperatingSystem();

    protected Harvest harvest = new BasicHarvest();

    public GenericGatherer() {
        Log.info("Instantiating '" + getClass() + "' gatherer");
    }

    public static PlatformEnum currentOS() {
        return SystemInfo.getCurrentPlatformEnum();
    }

    public static boolean isWindows() {
        return PlatformEnum.WINDOWS.equals(currentOS());
    }

    public static boolean isLinux() {
        return PlatformEnum.LINUX.equals(currentOS());
    }

    public static boolean isMACOSX() {
        return PlatformEnum.MACOSX.equals(currentOS());
    }

    public static boolean isUnknownOS() {
        return PlatformEnum.UNKNOWN.equals(currentOS());
    }

    public static boolean isSolaris() {
        return PlatformEnum.SOLARIS.equals(currentOS());
    }

    public static boolean isFreeBSD() {
        return PlatformEnum.FREEBSD.equals(currentOS());
    }

    public static String memoryInMB(long memoryInBytes) {
        return (memoryInBytes / 1024) + " MB";
    }

    public static String toGHz(long hz) {
        return (hz/ 1_000_000_000.0) + " GHz";
    }

    public abstract void gather();

    protected void logProperties() {
        Log.info(toString());
    }

    public Harvest getHarvest() {
        if (harvest.getPropertyCategories().isEmpty()) {
            gather();
        }
        return harvest;
    }

    @Override
    public String toString() {
        StringBuilder harvestLine = new StringBuilder();
        harvestLine
                .append("Gatherer ")
                .append(getClass().getCanonicalName())
                .append(" collected information:\n");

        harvest.getPropertyCategories().forEach((namespace, properties) -> {
            harvestLine
                    .append("\tNamespace: ")
                    .append(namespace)
                    .append("\n");
            properties.getProperties().forEach((name, value) -> {
                harvestLine
                        .append("\t\t")
                        .append(name + "=" + value)
                        .append("\n");
            });
        });

        return harvestLine.toString();
    }
}
