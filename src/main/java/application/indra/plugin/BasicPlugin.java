package application.indra.plugin;


import application.indra.gathering.Gatherer;
import application.indra.plugin.formatter.BasicTextFormatter;

import java.util.*;

public class BasicPlugin implements Plugin {

    private Map<String, Map<String, Object>> information = null;
    private Set<Gatherer> gatherers = new LinkedHashSet<>();
    private String name;
    private Formatter formatter;

    public BasicPlugin(final String name, final Formatter formatter) {
        this.name = name;
        this.formatter = formatter;
    }

    public BasicPlugin(final String name) {
        this.name = name;
        formatter = new BasicTextFormatter();
    }

    public String getName() {
        return name;
    }

    public void addGatherer(final Gatherer gatherer) {
        gatherers.add(gatherer);
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BasicPlugin that = (BasicPlugin) o;
        return information.equals(that.information) &&
                gatherers.equals(that.gatherers) &&
                name.equals(that.name) &&
                formatter.equals(that.formatter);
    }

    @Override
    public int hashCode() {
        return Objects.hash(information, gatherers, name, formatter);
    }

    @Override
    public Map<String, Map<String, Object>> getInfo() {
        if (information == null) {
            information = new LinkedHashMap<>();
            gatherers.forEach((gatherer) -> {
                final Map<String, Map<String, Object>> formattedInfo = formatter.format(gatherer);
                formattedInfo.forEach((namespace, pairsMap) -> {
                    information.put(namespace, pairsMap);
                });

            });

        }
        return information;
    }

}
