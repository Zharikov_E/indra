package application.indra.gathering.properties;

import application.indra.gathering.PropertiesSet;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

public class BasicPropertiesSet implements PropertiesSet {

    private Map<String, Object> properties = new LinkedHashMap<>();

    @Override
    public void setProperty(String propName, Object propValue) {
        properties.put(propName, propValue);
    }

    @Override
    public Object getPropertyValue(String propName) {
        return properties.get(propName);
    }

    @Override
    public void removeProperty(String propName) {
        properties.remove(propName);
    }

    @Override
    public Map<String, Object> getProperties() {
        return properties;
    }

    @Override
    public Set<String> getPropertyNames() {
        return properties.keySet();
    }

    @Override
    public String toString() {
        return "BasicPropertiesSet{" +
                "properties=" + properties +
                '}';
    }
}
