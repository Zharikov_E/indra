package application.indra.gathering.gatherer.hardware;

import application.indra.gathering.PropertiesSet;
import application.indra.gathering.gatherer.GenericGatherer;
import oshi.hardware.PowerSource;

public class PowerSourcesGatherer extends GenericGatherer {

    @Override
    public void gather() {
        gatherPowerSources(HARDWARE.getPowerSources());
        logProperties();
    }

    protected void gatherPowerSources(final PowerSource[] powerSources) {
        for (int i = 0; i < powerSources.length; i++) {
            final PowerSource powerSource = powerSources[i];
            if (!"Unknown".equals(powerSource.getName())) {
                PropertiesSet powerProps = harvest.getCategoryProperties("power-source-" + i);
                powerProps.setProperty("name", powerSource.getName());
                powerProps.setProperty("remaining-capacity", powerSource.getRemainingCapacity());
                powerProps.setProperty("time-remaining", powerSource.getTimeRemaining());
            }
        }
    }

}
