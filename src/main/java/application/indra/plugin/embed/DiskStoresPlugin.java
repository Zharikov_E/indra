package application.indra.plugin.embed;

import application.indra.gathering.gatherer.hardware.DiskStoresGatherer;
import application.indra.plugin.BasicPlugin;

public class DiskStoresPlugin extends BasicPlugin {

    public DiskStoresPlugin() {
        super("Disk-Stores");
        addGatherer(new DiskStoresGatherer());
    }

}
