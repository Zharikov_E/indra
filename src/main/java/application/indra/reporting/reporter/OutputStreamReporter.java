package application.indra.reporting.reporter;

import application.indra.plugin.BasicPlugin;
import application.indra.plugin.Plugin;
import application.indra.reporting.StreamReporter;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Map;
import java.util.Set;

public class OutputStreamReporter implements StreamReporter {

    private OutputStream dest;
    private Set<Plugin> plugins;

    public OutputStreamReporter(OutputStream dest, Set<Plugin> plugins) {
        this.dest = dest;
        this.plugins = plugins;
    }

    @Override
    public void setPlugins(Set<Plugin> plugins) {
        this.plugins = plugins;
    }

    @Override
    public void setOutputStream(OutputStream stream) {
        this.dest = stream;
    }

    @Override
    public void writeReport() {
        StringBuilder formattedInfo = new StringBuilder();


        plugins.forEach(plugin -> {
            formattedInfo
                    .append(((BasicPlugin) plugin).getName())
                    .append("\n");

            Map<String, Map<String, Object>> pluginInfo = plugin.getInfo();

            pluginInfo.forEach((category, properties) -> {
                formattedInfo
                        .append("\t")
                        .append(category)
                        .append("\n");

                properties.forEach((propName, value) -> {
                    formattedInfo
                            .append("\t\t")
                            .append(propName)
                            .append(" ")
                            .append(value)
                            .append("\n");
                });
            });
        });

        try {
            dest.write(formattedInfo.toString().getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}
