package application.indra.plugin.embed;

import application.indra.gathering.gatherer.hardware.CPUGatherer;
import application.indra.plugin.BasicPlugin;

public class CPUPlugin extends BasicPlugin {

    public CPUPlugin() {
        super("CPU");
        addGatherer(new CPUGatherer());
    }

}
