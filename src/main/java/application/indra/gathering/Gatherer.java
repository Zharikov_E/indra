package application.indra.gathering;

public interface Gatherer {

    void gather();

    Harvest getHarvest();

}
