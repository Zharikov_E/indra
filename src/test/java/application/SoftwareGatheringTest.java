package application;

import application.indra.gathering.gatherer.software.FileSystemGatherer;
import application.indra.gathering.gatherer.software.NetworkParamsGatherer;
import application.indra.gathering.gatherer.software.OperatingSystemGatherer;
import application.indra.gathering.gatherer.software.ProcessesGatherer;
import org.junit.Test;

public class SoftwareGatheringTest {

    @Test
    public void testFileSystemGatherer() {
        new FileSystemGatherer().gather();
    }

    @Test
    public void testNetworkParamsGatherer() {
        new NetworkParamsGatherer().gather();
    }

    @Test
    public void testOperatingSystemGatherer() {
        new OperatingSystemGatherer().gather();
    }

    @Test
    public void testProcessesGatherer() {
        new ProcessesGatherer().gather();
    }

}
