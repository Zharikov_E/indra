package application;

import application.indra.plugin.BasicPlugin;
import application.indra.plugin.Plugin;
import application.indra.plugin.embed.*;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.LinkedHashMap;
import java.util.Map;

@Controller
public class MainController implements ErrorController {

    private Map<String, Plugin> plugins = new LinkedHashMap<>();

    {
        addBasicPlugin(new CPUPlugin());
        addBasicPlugin(new PCSystemPlugin());
        addBasicPlugin(new NetworkingPlugin());
        addBasicPlugin(new SoundCardPlugin());
        addBasicPlugin(new DisplayPlugin());
        addBasicPlugin(new ProcessesPlugin());
        addBasicPlugin(new DiskStoresPlugin());
        addBasicPlugin(new FileSystemPlugin());
    }

    private void addBasicPlugin(final BasicPlugin plugin) {
        plugins.put(plugin.getName(), plugin);
    }

    private Map<String, Map<String, Object>> getPluginInfo(String pluginName) {
        return plugins.get(pluginName).getInfo();
    }

    @RequestMapping(value = "/plugin/{pluginName}", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody Object jsonGetInterface(@PathVariable(value = "pluginName") String pluginName) {
        return plugins.get(pluginName).getInfo();
    }

    @RequestMapping(value = {"{pluginName}"}, method = RequestMethod.GET)
    public String index(@PathVariable(value = "pluginName") String pluginName, Model model) {
        model.addAttribute("plugins", plugins);
        model.addAttribute("properties", getPluginInfo(pluginName));
        return "index";
    }

    @RequestMapping(value = {"/", ""}, method = RequestMethod.GET)
    public String welcome(Model model) {
        model.addAttribute("plugins", plugins);
        return "welcome";
    }

    @RequestMapping(value = "/error", method = RequestMethod.GET)
    public String error(Model model) {
        model.addAttribute("plugins", plugins);
        return "welcome";
    }

    @Override
    public String getErrorPath() {
        return "/error";
    }
}
