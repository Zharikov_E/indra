package application.indra.reporting;

import application.indra.plugin.Plugin;

import java.io.OutputStream;
import java.util.Set;

public interface StreamReporter {

    void setPlugins(final Set<Plugin> plugins);

    void setOutputStream(final OutputStream stream);

    void writeReport();

}
