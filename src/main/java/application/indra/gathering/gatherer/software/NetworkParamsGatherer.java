package application.indra.gathering.gatherer.software;

import application.indra.gathering.PropertiesSet;
import application.indra.gathering.gatherer.GenericGatherer;
import oshi.software.os.NetworkParams;

import static java.lang.System.setProperty;

public class NetworkParamsGatherer extends GenericGatherer {

    @Override
    public void gather() {
        final NetworkParams networkParams = SOFTWARE.getNetworkParams();
        gatherNetworkParams(networkParams);
        logProperties();
    }

    protected void gatherNetworkParams(final NetworkParams networkParams) {
        PropertiesSet networkProps = harvest.getCategoryProperties("network-params");
        networkProps.setProperty("domain-name", networkParams.getDomainName());
        networkProps.setProperty("host-name", networkParams.getHostName());
        networkProps.setProperty("ipv4-default-gateway", networkParams.getIpv4DefaultGateway());
        networkProps.setProperty("ipv6-defaul-gateway", networkParams.getIpv6DefaultGateway());
        final String[] dnsServers = networkParams.getDnsServers();
        for (int i = 0; i < dnsServers.length; i++) {
            final String dnsServer = dnsServers[i];
            networkProps.setProperty("dns-" + i, dnsServer);
        }
    }

}
