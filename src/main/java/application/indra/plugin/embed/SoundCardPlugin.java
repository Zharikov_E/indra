package application.indra.plugin.embed;

import application.indra.gathering.gatherer.hardware.SoundCardGatherer;
import application.indra.plugin.BasicPlugin;

public class SoundCardPlugin extends BasicPlugin {

    public SoundCardPlugin() {
        super("Sound-Cards");
        addGatherer(new SoundCardGatherer());
    }
}
