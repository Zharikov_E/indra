package application.indra.gathering.gatherer.hardware;

import application.indra.gathering.PropertiesSet;
import application.indra.gathering.gatherer.GenericGatherer;
import application.indra.gathering.properties.BasicPropertiesSet;
import oshi.hardware.HWDiskStore;
import oshi.hardware.HWPartition;

import static java.lang.System.setProperty;

public class DiskStoresGatherer extends GenericGatherer {

    public DiskStoresGatherer() {
    }

    @Override
    public void gather() {
        gatherDiskStores(HARDWARE.getDiskStores());
        logProperties();
    }

    protected void gatherDiskStores(final HWDiskStore[] diskStores) {
        for (int i = 0; i < diskStores.length; i++) {
            final HWDiskStore diskStore = diskStores[i];

            final String diskStoreNS = "disk-store-" + i;
            PropertiesSet disksProps = harvest.getCategoryProperties("disk-store-" + i);

            disksProps.setProperty("model", diskStore.getModel());
            disksProps.setProperty("name", diskStore.getName());
            disksProps.setProperty("current-queue-length", diskStore.getCurrentQueueLength());
            disksProps.setProperty("read-bytes", diskStore.getReadBytes());
            disksProps.setProperty("reads", diskStore.getReads());
            disksProps.setProperty("serial", diskStore.getSerial());
            disksProps.setProperty("size", diskStore.getSize());
            disksProps.setProperty("timestamp", diskStore.getTimeStamp());
            disksProps.setProperty("transfer-time", diskStore.getTransferTime());
            disksProps.setProperty("write-bytes", diskStore.getWriteBytes());
            disksProps.setProperty("writes", diskStore.getWrites());
            disksProps.setProperty("update-disk-stats", diskStore.updateDiskStats());
            disksProps.setProperty("partitions", gatherPartitions(diskStore));
        }
    }


    protected PropertiesSet gatherPartitions(final HWDiskStore diskStore) {
        HWPartition[] partitions = diskStore.getPartitions();
        BasicPropertiesSet diskPartProps = new BasicPropertiesSet();
        for (int i = 0; i < partitions.length; i++) {
            HWPartition partition = partitions[i];

            diskPartProps.setProperty("name", partition.getName());
            diskPartProps.setProperty("size", partition.getSize());
            diskPartProps.setProperty("identification", partition.getIdentification());
            diskPartProps.setProperty("major", partition.getMajor());
            diskPartProps.setProperty("minor", partition.getMinor());
            diskPartProps.setProperty("mount-point", partition.getMountPoint());
            diskPartProps.setProperty("type", partition.getType());
            diskPartProps.setProperty("uuid", partition.getUuid());
        }
        return diskPartProps;
    }

}
