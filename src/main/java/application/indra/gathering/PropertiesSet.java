package application.indra.gathering;

import java.util.Map;
import java.util.Set;

public interface PropertiesSet {

    void setProperty(final String propName, final Object propValue);

    Object getPropertyValue(final String propName);

    void removeProperty(final String propName);

    Set<String> getPropertyNames();

    Map<String, Object> getProperties();

}
