package application.indra.plugin;

import application.indra.gathering.Gatherer;

import java.util.Map;

public interface Formatter {
    Map<String, Map<String, Object>> format(Gatherer gatherer);
}
