package application.indra.gathering.gatherer.software;

import application.indra.gathering.PropertiesSet;
import application.indra.gathering.gatherer.GenericGatherer;
import oshi.PlatformEnum;
import oshi.software.os.OperatingSystemVersion;

public class OperatingSystemGatherer extends GenericGatherer {

    @Override
    public void gather() {
        gatherOSInfo();
        logProperties();
    }

    protected void gatherOSInfo() {
        PropertiesSet osProps = harvest.getCategoryProperties("operating-system");
        final OperatingSystemVersion osVersion = SOFTWARE.getVersion();
        osProps.setProperty("family", SOFTWARE.getFamily());
        osProps.setProperty("manufacturer", SOFTWARE.getManufacturer());
        osProps.setProperty("bitness", SOFTWARE.getBitness());
        osProps.setProperty("build-number", osVersion.getBuildNumber());
        osProps.setProperty("code-name", osVersion.getCodeName());
        osProps.setProperty("version", osVersion.getVersion());
        osProps.setProperty("current-user", System.getProperty("user.name"));
        osProps.setProperty("current-user-directory", System.getProperty("user.home"));
    }

}
