package application.indra.gathering.gatherer.software;

import application.indra.gathering.PropertiesSet;
import application.indra.gathering.gatherer.GenericGatherer;
import oshi.software.os.OSProcess;
import oshi.software.os.OperatingSystem;

public class ProcessesGatherer extends GenericGatherer {

    @Override
    public void gather() {
        gatherProcesses();
        logProperties();
    }

    protected void gatherProcesses() {
        final int threadCount = SOFTWARE.getThreadCount();
        final int processCount = SOFTWARE.getProcessCount();

        PropertiesSet processesProps = harvest.getCategoryProperties("processes");
        processesProps.setProperty("thread-count", threadCount);
        processesProps.setProperty("process-count", processCount);

        final OSProcess[] processes = SOFTWARE.getProcesses(processCount,
                OperatingSystem.ProcessSort.PID);

        for (int i = 0; i < processes.length; i++) {
            final OSProcess process = processes[i];

            PropertiesSet procProps = harvest.getCategoryProperties("process-" + i);
            procProps.setProperty("name", process.getName());
            procProps.setProperty("user", process.getUser());
            procProps.setProperty("thread-count", process.getThreadCount());
            procProps.setProperty("cpu-percent", process.calculateCpuPercent());
            procProps.setProperty("bytes-read", process.getBytesRead());
            procProps.setProperty("bytes-written", process.getBytesWritten());
            procProps.setProperty("command-line", process.getCommandLine());
            procProps.setProperty("working-directory", process.getCurrentWorkingDirectory());
            procProps.setProperty("group", process.getGroup());
            procProps.setProperty("group-id", process.getGroupID());
            procProps.setProperty("kernel-time", process.getKernelTime());
            procProps.setProperty("open-files", process.getOpenFiles());
            procProps.setProperty("process-id", process.getProcessID());
            procProps.setProperty("parent-process-id", process.getParentProcessID());
            procProps.setProperty("priority", process.getPriority());
            procProps.setProperty("path", process.getPath());
            procProps.setProperty("resident-set-size", process.getResidentSetSize());
            procProps.setProperty("start-time", process.getStartTime());
            procProps.setProperty("state", process.getState());
            procProps.setProperty("up-time", process.getUpTime());
            procProps.setProperty("user-id", process.getUserID());
            procProps.setProperty("user-time", process.getUserTime());
            procProps.setProperty("virtual-size", process.getVirtualSize());
        }
    }

}
