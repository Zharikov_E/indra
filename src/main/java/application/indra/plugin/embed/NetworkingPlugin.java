package application.indra.plugin.embed;

import application.indra.gathering.gatherer.hardware.NetworkInterfacesGatherer;
import application.indra.gathering.gatherer.software.NetworkParamsGatherer;
import application.indra.plugin.BasicPlugin;

public class NetworkingPlugin extends BasicPlugin {

    public NetworkingPlugin() {
        super("Networking");
        addGatherer(new NetworkInterfacesGatherer());
        addGatherer(new NetworkParamsGatherer());
    }
}
