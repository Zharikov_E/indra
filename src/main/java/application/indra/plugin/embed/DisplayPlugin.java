package application.indra.plugin.embed;

import application.indra.gathering.gatherer.hardware.DisplaysGatherer;
import application.indra.plugin.BasicPlugin;

public class DisplayPlugin extends BasicPlugin {

    public DisplayPlugin() {
        super("Diplays");
        addGatherer(new DisplaysGatherer());
    }
}
