package application.indra.gathering.gatherer.hardware;

import application.indra.gathering.PropertiesSet;
import application.indra.gathering.gatherer.GenericGatherer;
import oshi.hardware.Display;

import java.awt.*;

public class DisplaysGatherer extends GenericGatherer {

    @Override
    public void gather() {
        final Display[] displays = HARDWARE.getDisplays();

        gatherEdidInfo(displays);
        logProperties();
    }

    protected void gatherEdidInfo(final Display[] displays) {
        for (int i = 0; i < displays.length; i++) {
            final Display display = displays[i];
            PropertiesSet displayProps = harvest.getCategoryProperties("display-" + i);
            displayProps.setProperty("edid-info", display.toString());
        }
    }

}
