package application.indra.plugin.embed;

import application.indra.gathering.gatherer.hardware.ComputerSystemGatherer;
import application.indra.gathering.gatherer.hardware.MemoryGatherer;
import application.indra.gathering.gatherer.software.OperatingSystemGatherer;
import application.indra.plugin.BasicPlugin;

public class PCSystemPlugin extends BasicPlugin {

    public PCSystemPlugin() {
        super("Computer-System");
        addGatherer(new OperatingSystemGatherer());
        addGatherer(new ComputerSystemGatherer());
        addGatherer(new MemoryGatherer());
    }

}
