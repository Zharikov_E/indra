package application.indra.gathering.gatherer.hardware;

import application.indra.gathering.PropertiesSet;
import application.indra.gathering.gatherer.GenericGatherer;
import oshi.hardware.SoundCard;

public class SoundCardGatherer extends GenericGatherer {

    @Override
    public void gather() {
        gatherSoundCards(HARDWARE.getSoundCards());
        logProperties();
    }

    protected void gatherSoundCards(final SoundCard[] soundCards) {
        for (int i = 0; i < soundCards.length; i++) {
            final SoundCard soundCard = soundCards[i];

            PropertiesSet soundProps = harvest.getCategoryProperties("sound-card-" + i);
            soundProps.setProperty("name", soundCard.getName());
            soundProps.setProperty("codec", soundCard.getCodec());
            soundProps.setProperty("driver-version", soundCard.getDriverVersion());
        }
    }

}
