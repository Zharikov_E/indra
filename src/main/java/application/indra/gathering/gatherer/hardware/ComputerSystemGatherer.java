package application.indra.gathering.gatherer.hardware;

import application.indra.gathering.PropertiesSet;
import application.indra.gathering.gatherer.GenericGatherer;
import oshi.hardware.Baseboard;
import oshi.hardware.ComputerSystem;
import oshi.hardware.Firmware;

public class ComputerSystemGatherer extends GenericGatherer {

    @Override
    public void gather() {
        final ComputerSystem hwComputerSystem = HARDWARE.getComputerSystem();
        gatherBaseboard(hwComputerSystem.getBaseboard());
        gatherFirmware(hwComputerSystem.getFirmware());
        logProperties();
    }

    protected void gatherFirmware(final Firmware hwFirmware) {
        PropertiesSet firmwareProps = harvest.getCategoryProperties("firmware");

        firmwareProps.setProperty("description", hwFirmware.getDescription());
        firmwareProps.setProperty("manufacturer", hwFirmware.getManufacturer());
        firmwareProps.setProperty("name", hwFirmware.getName());
        firmwareProps.setProperty("release-date", hwFirmware.getReleaseDate());
        firmwareProps.setProperty("version", hwFirmware.getVersion());
    }

    protected void gatherBaseboard(final Baseboard hwBaseboard) {
        PropertiesSet baseboardProps = harvest.getCategoryProperties("baseboard");
        baseboardProps.setProperty("manufacturer", hwBaseboard.getManufacturer());
        baseboardProps.setProperty("model", hwBaseboard.getModel());
        baseboardProps.setProperty("serial-number", hwBaseboard.getSerialNumber());
        baseboardProps.setProperty("version", hwBaseboard.getVersion());
    }

}
