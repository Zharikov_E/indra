package application.indra.gathering.gatherer.hardware;

import application.indra.gathering.PropertiesSet;
import application.indra.gathering.gatherer.GenericGatherer;
import oshi.hardware.CentralProcessor;

public class CPUGatherer extends GenericGatherer {

    @Override
    public void gather() {
        gatherCpu(HARDWARE.getProcessor());
        logProperties();
    }

    protected void gatherCpu(final CentralProcessor hwProcessor) {
        PropertiesSet cpuProps = harvest.getCategoryProperties("cpu");
        cpuProps.setProperty("processor-id", hwProcessor.getProcessorID());
        cpuProps.setProperty("vendor", hwProcessor.getVendor());
        cpuProps.setProperty("name", hwProcessor.getName());
        cpuProps.setProperty("identifier", hwProcessor.getIdentifier());
        cpuProps.setProperty("model", hwProcessor.getModel());
        cpuProps.setProperty("family", hwProcessor.getFamily());
        cpuProps.setProperty("is-cpu-64-bit", hwProcessor.isCpu64bit());
        cpuProps.setProperty("logical-processors", hwProcessor.getLogicalProcessorCount());
        cpuProps.setProperty("physical-processors", hwProcessor.getPhysicalProcessorCount());
        cpuProps.setProperty("vendor-freq", toGHz(hwProcessor.getVendorFreq()));
        cpuProps.setProperty("stepping", hwProcessor.getStepping());
    }

}
