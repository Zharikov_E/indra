package application.indra.gathering;

import java.util.Map;
import java.util.Set;

public interface Harvest {

    void setCategory(final String category, final PropertiesSet props);

    PropertiesSet getCategoryProperties(final String category);

    void removeCategory(final String category);

    Set<String> getCategoryNames();

    Map<String, PropertiesSet> getPropertyCategories();

}
