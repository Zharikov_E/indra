package application.indra.plugin.embed;

import application.indra.gathering.gatherer.software.FileSystemGatherer;
import application.indra.plugin.BasicPlugin;

public class FileSystemPlugin extends BasicPlugin {

    public FileSystemPlugin() {
        super("File-System");
        addGatherer(new FileSystemGatherer());
    }
}
