package application.indra.gathering.gatherer.software;

import application.indra.gathering.PropertiesSet;
import application.indra.gathering.gatherer.GenericGatherer;
import oshi.software.os.FileSystem;
import oshi.software.os.OSFileStore;

public class FileSystemGatherer extends GenericGatherer {

    @Override
    public void gather() {
        final FileSystem fileSystem = SOFTWARE.getFileSystem();
        PropertiesSet fileSystemProps = harvest.getCategoryProperties("file-system");
        fileSystemProps.setProperty("max-file-descriptors", fileSystem.getMaxFileDescriptors());
        fileSystemProps.setProperty("open-file-descriptors", fileSystem.getOpenFileDescriptors());
        gatherFileStores(fileSystem);
        logProperties();
    }

    protected void gatherFileStores(final FileSystem fileSystem) {
        final OSFileStore[] fileStores = fileSystem.getFileStores();

        for (int i = 0; i < fileStores.length; i++) {
            final OSFileStore fileStore = fileStores[i];

            PropertiesSet fileStoreProps = harvest.getCategoryProperties("file-store-" + i);
            fileStoreProps.setProperty("name", fileStore.getName());
            fileStoreProps.setProperty("description", fileStore.getDescription());
            fileStoreProps.setProperty("uuid", fileStore.getUUID());
            fileStoreProps.setProperty("volume", fileStore.getVolume());
            fileStoreProps.setProperty("type", fileStore.getType());
            fileStoreProps.setProperty("total-space", fileStore.getTotalSpace());
            fileStoreProps.setProperty("usable-space", fileStore.getUsableSpace());
            fileStoreProps.setProperty("logical-volume", fileStore.getLogicalVolume());
            fileStoreProps.setProperty("mount", fileStore.getMount());
            fileStoreProps.setProperty("free-inodes", fileStore.getFreeInodes());
            fileStoreProps.setProperty("total-inodes", fileStore.getTotalInodes());
        }
    }

}
