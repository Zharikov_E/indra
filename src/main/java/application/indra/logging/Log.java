package application.indra.logging;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public final class Log {

    public static final Logger LOGGER = LoggerFactory.getLogger(Log.class);

    public static void info(String msg) {
        LOGGER.info(msg);
    }

    public static void trace(String msg) {
        LOGGER.trace(msg);
    }

    public static void debug(String msg) {
        LOGGER.debug(msg);
    }

    public static void warn(String msg) {
        LOGGER.warn(msg);
    }

    public static void error(String msg) {
        LOGGER.error(msg);
    }

}
