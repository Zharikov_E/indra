package application;

import application.indra.gathering.gatherer.hardware.*;
import org.junit.Test;

public class HardwareGatheringTest {

	@Test
	public void testComputerSystemGatherer() {
		new ComputerSystemGatherer().gather();
	}

	@Test
	public void testCpuGatherer() {
		new CPUGatherer().gather();
	}

	@Test
	public void testDiskStoresGatherer() {
		new DiskStoresGatherer().gather();
	}

	@Test
	public void testDisplaysGatherer() {
		new DisplaysGatherer().gather();
	}

	@Test
	public void testMemoryGatherer() {
		new MemoryGatherer().gather();
	}

	@Test
	public void testNetworkInterfacesGatherer() {
		new NetworkInterfacesGatherer().gather();
	}

	@Test
	public void testPowerSourcesGatherer() {
		new PowerSourcesGatherer().gather();
	}

	@Test
	public void testSoundCardGatherer() {
		new SoundCardGatherer().gather();
	}

	@Test
	public void testUSBDevicesGatherer() {
		new USBDevicesGatherer().gather();
	}

}
